const colorList = [
  "pallet",
  "viridian",
  "pewter",
  "cerulean",
  "vermillion",
  "lavender",
  "celadon",
  "saffron",
  "fuschia",
  "cinnabar",
];
let container = document.querySelector("#colorContainer");
let renderColor = () => {
  for (let i = 0; i < colorList.length; i++) {
    container.innerHTML +=
      0 == i
        ? "<button class ='color-button " + colorList[i] + " active'></button>"
        : "<button class ='color-button " + colorList[i] + "'></button>";
  }
};
renderColor();

let colorPicker = document.querySelectorAll(".color-button"),
  house = document.querySelector("#house");

let changeColor = (r, a) => {
  for (let r = 0; r < colorPicker.length; r++) {
    colorPicker[r].classList.remove("active");
  }
  colorPicker[a].classList.add("active"), (house.className = "house " + r);
};

for (let i = 0; i < colorPicker.length; i++) {
  colorPicker[i].addEventListener("click", () => {
    changeColor(colorList[i], i);
  });
}
