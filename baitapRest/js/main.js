function getEle(tag) {
  return document.querySelector(tag);
}
let tinhDTB = (...diem) => {
  let sum = 0;
  return (
    diem.map((diem) => {
      sum += parseFloat(diem);
    }),
    (sum / diem.length).toFixed(2)
  );
};

getEle("#btnKhoi1").onclick = () => {
  let t = getEle("#inpToan").value,
    l = getEle("#inpLy").value,
    h = getEle("#inpHoa").value;
  getEle("#tbKhoi1").innerHTML = tinhDTB(t, l, h);
};

getEle("#btnKhoi2").onclick = () => {
  let v = getEle("#inpVan").value,
    s = getEle("#inpSu").value,
    d = getEle("#inpDia").value,
    e = getEle("#inpEnglish").value;
  getEle("#tbKhoi2").innerHTML = tinhDTB(v, s, d, e);
};
