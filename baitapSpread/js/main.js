const heading = document.querySelector(".heading");

let jump = (char) => {
  return [...char].map((char) => `<span>${char}</span>`).join("");
};

heading.innerHTML = jump(heading.innerText);
console.log(jump(heading.innerText));
